import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyGallery(),
    );
  }
}

class MyGallery extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Gallery'),
      ),
      body: FutureBuilder<List<Photo>>(
        future: fetchPhotos(http.Client()),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            print(snapshot.error);
          }
          return snapshot.hasData
              ? PhotosList(photos: snapshot.data)
              : Center(child: CircularProgressIndicator());
        },
      ),
    );
  }
}

class PhotosList extends StatelessWidget {
  final List<Photo> photos;

  PhotosList({Key key, this.photos}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: const EdgeInsets.all(8),
      itemCount: photos.length,
      itemBuilder: (context, index) {
        String description = photos[index].altDescription != null ?
        photos[index].altDescription : '';
        String firstName = photos[index].firstName != null ?
        photos[index].firstName : '';
        String lastName = photos[index].lastName != null ?
        photos[index].lastName : '';
        String author = firstName + ' ' + lastName;
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return FullImage(photos[index].urlRegularImage);
              }),
            );
          },
          child: Center(
            child: Column(
              children: <Widget>[
                Image.network(photos[index].urlSmallImage, width: 300),
                Text(
                    description, style: TextStyle(fontWeight: FontWeight.bold)),
                Text(author, style: TextStyle(color: Colors.grey[500]))
              ],
            ),
          ),
        );
      },
    );
  }
}

class FullImage extends StatelessWidget{

  final String urlRegularImage;

  FullImage(this.urlRegularImage);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Image.network(urlRegularImage),
        ),
      ),
    );
  }

}



class Photo {

  final String description;
  final String altDescription;
  final String urlSmallImage;
  final String urlFullImage;
  final String urlThumbImage;
  final String urlRegularImage;
  final String firstName;
  final String lastName;

  Photo({this.description, this.altDescription, this.urlSmallImage,
    this.urlFullImage, this.urlThumbImage, this.urlRegularImage,
    this.firstName, this.lastName});

  factory Photo.fromJson(Map<String, dynamic> data) {
    return Photo(
        description: data['description'] as String,
        altDescription: data['alt_description'] as String,
        urlSmallImage: data['urls']['small'] as String,
        urlFullImage: data['urls']['full'] as String,
        urlThumbImage: data['urls']['thumb'] as String,
        urlRegularImage: data['urls']['regular'] as String,
        firstName: data['user']['first_name'] as String,
        lastName: data['user']['last_name'] as String);
  }
}

List<Photo> getPhotoFromJson(String response) {
  final parsedData = jsonDecode(response).cast<Map<String,dynamic>>();
  return parsedData.map<Photo>((data) => Photo.fromJson(data)).toList();
}

Future<List<Photo>> fetchPhotos(http.Client client) async {
  final response =
  await client.get('https://api.unsplash.com/photos/?client_id='
      '896d4f52c589547b2134bd75ed48742db637fa51810b49b607e37e46ab2c0043');
  return compute(getPhotoFromJson, response.body);
}


